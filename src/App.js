import React, { Component} from 'react';
import logo from './logo.svg';
import './App.css';

const PATH_BASE = 'https://hn.algolia.com/api/v1';
const PATH_SEARCH = '/search';
const PARAM_SEARCH = 'query=';
const DEFAULT_QUERY = 'redux';

const isSearched = (query) => (item) => !query || item.title.toLowerCase().indexOf(query.toLowerCase()) !== -1;


class Table extends Component {
  render() {
    const { list, pattern } = this.props;
    return (
      <div className="table">
        { list.filter(isSearched(pattern)).map((item) =>
        <div key={item.objectID} className="table-row">
            <span style={{ width: '40%' }}><a href={item.url}>{item.title}</a></span>
            <span style={{ width: '30%' }}>{item.author}</span>
            <span style={{ width: '15%' }}>{item.num_comments}</span>
            <span style={{ width: '15%' }}>{item.points}</span>
       </div>
      )}
      </div>
    );
  }
}


function Search(props) {
  const { value, onChange, children } = props;
    return (
    <form>
    {children} <input type="text" value={value} onChange={onChange} />
    </form>
    );
}


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      result: null,
      query: DEFAULT_QUERY,
    };

    this.setSearchTopstories = this.setSearchTopstories.bind(this);
    this.fetchSearchTopstories = this.fetchSearchTopstories.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
  }

  setSearchTopstories(result) {
    this.setState({ result });
  }

  fetchSearchTopstories(query) {
    fetch(`${PATH_BASE}${PATH_SEARCH}?${PARAM_SEARCH}${query}`)
    .then(response => response.json())
    .then(result => this.setSearchTopstories(result));
  }

  componentDidMount() {
    const { query } = this.state;
    this.fetchSearchTopstories(query);
  }


  onSearchChange(event) {
    this.setState({ query: event.target.value });
    console.log('Internal state has been changed: '+this.state.query);
 }

  render() {
   const { query, result } = this.state;
    return (
      <div className="page">
        <div className="interactions">
          <Search value={query} onChange={this.onSearchChange}>Search</Search>
        </div>
        { result ? <Table list={result.hits} pattern={query} /> : null }
      </div>
    );
  }
}

export default App;
